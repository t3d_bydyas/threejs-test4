export default [
    {
        name: 'waterNormal',
        type: 'texture',
        path: '/textures/waternormals.jpg',
    },
    {
        name: 'terrainModel',
        type: 'model',
        path: '/models/terrain.glb',
    },
]
