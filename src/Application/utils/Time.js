import * as THREE from 'three'

import EventEmitter from './EventEmitter.js'

export default class Time extends EventEmitter {
    constructor() {
        super()

        // Setup
        this.clock = new THREE.Clock()
        this.oldElapsedTime = 0

        window.requestAnimationFrame(() => this.tick())
    }

    tick() {
        const elapsedTime = this.clock.getElapsedTime()
        this.delta = elapsedTime - this.oldElapsedTime
        this.oldElapsedTime = elapsedTime

        this.trigger('tick')

        window.requestAnimationFrame(() => this.tick())
    }
}
