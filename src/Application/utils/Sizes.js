import EventEmitter from './EventEmitter.js'

export default class Sizes extends EventEmitter {
    PIXEL_RATIO_LIMIT = 2

    constructor() {
        super()

        // Setup
        this.width = window.innerWidth
        this.height = window.innerHeight
        this.pixelRatio = Math.min(window.devicePixelRatio, this.PIXEL_RATIO_LIMIT)

        // Resize
        window.addEventListener('resize', () => {
            this.resize()
            this.trigger('resize')
        })
    }

    resize() {
        this.width = window.innerWidth
        this.height = window.innerHeight
        this.pixelRatio = Math.min(window.devicePixelRatio, this.PIXEL_RATIO_LIMIT)
    }
}
