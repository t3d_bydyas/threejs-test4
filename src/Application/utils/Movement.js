import * as THREE from 'three'

import Application from '../Application'

export default class Movement {
    RAY_LENGTH = 25
    SPEED_BOOST = 10
    SLOW_INDEX = 0.5

    constructor() {
        this.application = new Application()

        // Setup
        this.camera = this.application.camera
        this.scene = this.application.scene
        this.time = this.application.time

        this.raycaster = new THREE.Raycaster()
        this.velocity = new THREE.Vector3()
        this.direction = new THREE.Vector3()
        this.lastPosition = new THREE.Vector3()

        this.moveForward = false
        this.moveBackward = false
        this.moveLeft = false
        this.moveRight = false

        // Listeners
        document.addEventListener('keydown', (event) => this.onKeyDown(event))
        document.addEventListener('keyup', (event) => this.onKeyUp(event))
    }

    moveY() {
        const controlsPosition = this.camera.controls.getObject().position

        this.raycaster.set(controlsPosition, new THREE.Vector3())
        this.raycaster.ray.direction.y = this.RAY_LENGTH
        this.raycaster.ray.origin.y -= this.RAY_LENGTH

        const intersections = this.raycaster.intersectObjects(this.scene.children)
        if (intersections.length) {
            const intersectedY = intersections[0].point.y
            controlsPosition.y = intersectedY + this.camera.EYE_HEIGHT

            if (intersectedY <= 0) {
                controlsPosition.copy(this.lastPosition)
            }

            this.lastPosition.copy(controlsPosition)
        }
    }

    moveXZ() {
        this.velocity.z -= this.velocity.z * (this.SPEED_BOOST * this.SLOW_INDEX) * this.time.delta
        this.velocity.x -= this.velocity.x * (this.SPEED_BOOST * this.SLOW_INDEX) * this.time.delta

        this.direction.z = Number(this.moveForward) - Number(this.moveBackward)
        this.direction.x = Number(this.moveRight) - Number(this.moveLeft)
        this.direction.normalize()

        if (this.moveForward || this.moveBackward) {
            this.velocity.z -= this.direction.z * this.SPEED_BOOST * this.time.delta
        }
        if (this.moveLeft || this.moveRight) {
            this.velocity.x -= this.direction.x * this.SPEED_BOOST * this.time.delta
        }

        this.camera.controls.moveRight(-this.velocity.x)
        this.camera.controls.moveForward(-this.velocity.z)
    }

    onKeyDown(event) {
        switch (event.code) {
            case 'ArrowUp':
            case 'KeyW':
                this.moveForward = true
                break
            case 'ArrowLeft':
            case 'KeyA':
                this.moveLeft = true
                break
            case 'ArrowDown':
            case 'KeyS':
                this.moveBackward = true
                break
            case 'ArrowRight':
            case 'KeyD':
                this.moveRight = true
                break
        }
    }

    onKeyUp(event) {
        switch (event.code) {
            case 'ArrowUp':
            case 'KeyW':
                this.moveForward = false
                break
            case 'ArrowLeft':
            case 'KeyA':
                this.moveLeft = false
                break
            case 'ArrowDown':
            case 'KeyS':
                this.moveBackward = false
                break
            case 'ArrowRight':
            case 'KeyD':
                this.moveRight = false
                break
        }
    }

    update() {
        if (this.camera.controls.isLocked) {
            this.moveY()
            this.moveXZ()
        }
    }
}
