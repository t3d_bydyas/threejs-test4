import Application from '../Application'

export default class UI {
    constructor() {
        this.application = new Application()

        // Setup
        this.camera = this.application.camera
        this.menu = document.querySelector('.menu')

        // Listeners
        this.menu.addEventListener('click', () => {
            this.camera.controls.lock()
        })
        this.camera.controls.addEventListener('lock', () => (this.menu.style.display = 'none'))
        this.camera.controls.addEventListener('unlock', () => (this.menu.style.display = 'flex'))
    }
}
