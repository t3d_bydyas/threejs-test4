import * as THREE from 'three'

import Application from '../Application'

export default class Environment {
    constructor() {
        this.application = new Application()

        // Setup
        this.scene = this.application.scene

        this.setBaseLight()
        this.setSunLight()
    }

    setBaseLight() {
        this.baseLight = new THREE.AmbientLight('#ffffff', 1)
        this.scene.add(this.baseLight)
    }

    setSunLight() {
        this.sunLight = new THREE.DirectionalLight('#FF8100', 3)
        this.sunLight.position.set(50, 100, 10)
        this.scene.add(this.sunLight)
    }
}
