import * as THREE from 'three'
import { Water } from 'three/addons/objects/Water.js'

import Application from '../Application.js'

export default class Ocean {
    constructor() {
        this.application = new Application()

        // Setup
        this.scene = this.application.scene
        this.world = this.application.world
        this.resources = this.application.resources

        this.setGeometry()
        this.setTextures()
        this.setMesh()
    }

    setGeometry() {
        this.geometry = new THREE.PlaneGeometry(1000, 1000)
    }

    setTextures() {
        this.textures = {}

        this.textures.normal = this.resources.items.waterNormal
        this.textures.normal.wrapS = THREE.RepeatWrapping
        this.textures.normal.wrapT = THREE.RepeatWrapping
    }

    setMesh() {
        this.mesh = new Water(this.geometry, {
            textureWidth: 512,
            textureHeight: 512,
            waterNormals: this.textures.normal,
            sunDirection: new THREE.Vector3(),
            sunColor: this.world.environment.sunLight.color,
            waterColor: 0x5f9e0,
            distortionScale: 3.7,
            fog: this.scene.fog !== undefined,
        })

        this.mesh.rotation.x = -Math.PI / 2

        this.scene.add(this.mesh)
    }

    update() {
        this.mesh.material.uniforms['time'].value += 1.0 / 60.0
    }
}
