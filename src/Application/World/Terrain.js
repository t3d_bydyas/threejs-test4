import Application from '../Application'

export default class Terrain {
    constructor() {
        this.application = new Application()

        // Setup
        this.scene = this.application.scene
        this.resources = this.application.resources

        this.setModel()
    }

    setModel() {
        this.model = this.resources.items.terrainModel.scene
        this.model.scale.set(10, 10, 10)
        this.model.position.y = 1
        this.scene.add(this.model)
    }
}
