import Application from '../Application'
import Environment from './Environment'
import Ocean from './Ocean'
import Terrain from './Terrain'

export default class World {
    constructor() {
        this.application = new Application()

        // Setup
        this.resources = this.application.resources
        this.scene = this.application.scene

        // Listener
        this.resources.on('loaded', () => {
            // World's objects
            this.environment = new Environment()
            this.ocean = new Ocean()
            this.terrain = new Terrain()
        })
    }

    update() {
        if (this.ocean) {
            this.ocean.update()
        }
    }
}
